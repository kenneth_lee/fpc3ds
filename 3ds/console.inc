//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved

  type

    ConsolePrint = function (con:pointer; c:cint):bool;cdecl;
  {! a font struct for the console. }
  {!< A pointer to the font graphics }
  {!<  Offset to the first valid character in the font table }
  {!< Number of characters in the font graphics }

    ConsoleFont = record
        gfx : pu8;
        asciiOffset : u16;
        numChars : u16;
      end;
    PConsoleFont  = ^ConsoleFont; 

  {* \brief console structure used to store the state of a console render context.
  
  Default values from consoleGetDefault();
  <div class="fixedFont"><pre>
  PrintConsole defaultConsole =
  
  	//Font:
  	
  		(u8*)default_font_bin, //font gfx
  		0, //first ascii character in the set
  		128, //number of characters in the font set
  	,
  	0,0, //cursorX cursorY
  	0,0, //prevcursorX prevcursorY
  	40, //console width
  	30, //console height
  	0,  //window x
  	0,  //window y
  	32, //window width
  	24, //window height
  	3, //tab size
  	0, //font character offset
  	0,  //print callback
  	false //console initialized
  ;
  </pre></div>
   }
  {!< font of the console. }
  {!< framebuffer address. }
  {!< Current X location of the cursor (as a tile offset by default)  }
  {!< Current Y location of the cursor (as a tile offset by default)  }
  {!< Internal state  }
  {!< Internal state  }
  {!< Width of the console hardware layer in characters  }
  {!< Height of the console hardware layer in characters   }
  {!< Window X location in characters (not implemented)  }
  {!< Window Y location in characters (not implemented)  }
  {!< Window width in characters (not implemented)  }
  {!< Window height in characters (not implemented)  }
  {!< Size of a tab }
  {!< foreground color }
  {!< background color }
  {!< reverse/bright flags }
  {!< callback for printing a character. Should return true if it has handled rendering the graphics
  									(else the print engine will attempt to render via tiles)  }
  {!< True if the console is initialized  }

    PrintConsole = record
        font : ConsoleFont;
        frameBuffer : pu16;
        cursorX : cint;
        cursorY : cint;
        prevCursorX : cint;
        prevCursorY : cint;
        consoleWidth : cint;
        consoleHeight : cint;
        windowX : cint;
        windowY : cint;
        windowWidth : cint;
        windowHeight : cint;
        tabSize : cint;
        fg : cint;
        bg : cint;
        flags : cint;
        PrintChar : ConsolePrint;
        consoleInitialised : bool;
      end;
    PPrintConsole  = ^PrintConsole;

  const
    CONSOLE_COLOR_BOLD = 1 shl 0;    
    CONSOLE_COLOR_FAINT = 1 shl 1;    
    CONSOLE_ITALIC = 1 shl 2;    
    CONSOLE_UNDERLINE = 1 shl 3;    
    CONSOLE_BLINK_SLOW = 1 shl 4;    
    CONSOLE_BLINK_FAST = 1 shl 5;    
    CONSOLE_COLOR_REVERSE = 1 shl 6;    
    CONSOLE_CONCEAL = 1 shl 7;    
    CONSOLE_CROSSED_OUT = 1 shl 8;    
  {! Console debug devices supported by libnds. }
  {!< swallows prints to stderr }
  {!< Directs stderr debug statements to 3dmoo }
  {!< Directs stderr debug statements to 3DS console window }

  type
    debugDevice = (debugDevice_NULL,debugDevice_3DMOO,debugDevice_CONSOLE
      );
  {!	\brief Loads the font into the console
  	\param console pointer to the console to update, if NULL it will update the current console
  	\param font the font to load
   }

  procedure consoleSetFont(console:PPrintConsole; font:PConsoleFont);cdecl;external;

  {!	\brief Sets the print window
  	\param console console to set, if NULL it will set the current console window
  	\param x x location of the window
  	\param y y location of the window
  	\param width width of the window
  	\param height height of the window
   }
  procedure consoleSetWindow(console:PPrintConsole; x:cint; y:cint; width:cint; height:cint);cdecl;external;

  {!	\brief Gets a pointer to the console with the default values
  	this should only be used when using a single console or without changing the console that is returned, other wise use consoleInit()
  	\return A pointer to the console with the default values
   }
  function consoleGetDefault:PPrintConsole;cdecl;external;

  {!	\brief Make the specified console the render target
  	\param console A pointer to the console struct (must have been initialized with consoleInit(PrintConsole* console)
  	\return a pointer to the previous console
   }
  function consoleSelect(console:PPrintConsole):PPrintConsole;cdecl;external;

  {!	\brief Initialise the console.
  	\param screen The screen to use for the console
  	\param console A pointer to the console data to initialze (if it's NULL, the default console will be used)
  	\return A pointer to the current console.
   }
  function consoleInit(screen:gfxScreen_t; console:PPrintConsole):PPrintConsole;cdecl;external;

  {!	\brief Initializes debug console output on stderr to the specified device
  	\param device The debug device (or devices) to output debug print statements to
   }
  procedure consoleDebugInit(device:debugDevice);cdecl;external;

  {! Clears the screan by using iprintf("\x1b[2J"); }
  procedure consoleClear;cdecl;external;

