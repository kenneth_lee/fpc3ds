//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved
//

  type
    float24Uniform_s = record
        id : u32;
        data : array[0..2] of u32;
      end;
  { this structure describes an instance of either a vertex or geometry shader }

    shaderInstance_s = record
        dvle : ^DVLE_s;
        boolUniforms : u16;
        intUniforms : array[0..3] of u32;
        float24Uniforms : ^float24Uniform_s;
        numFloat24Uniforms : u8;
      end;
    PshaderInstance_s  = ^shaderInstance_s
  { this structure describes an instance of a full shader program }

    shaderProgram_s = record
        vertexShader : ^shaderInstance_s;
        geometryShader : ^shaderInstance_s;
        geometryShaderInputStride : u8;
      end;
    PshaderProgram_s  = ^shaderProgram_s;

  function shaderInstanceInit(si:PshaderInstance_s; dvle:PDVLE_s):s32;cdecl;external;
  function shaderInstanceFree(si:PshaderInstance_s):s32;cdecl;external;
  function shaderInstanceSetBool(si:PshaderInstance_s; id:cint; value:bool):s32;cdecl;external;
  function shaderInstanceGetBool(si:PshaderInstance_s; id:cint; value:Pbool):s32;cdecl;external;
  function shaderInstanceGetUniformLocation(si:PshaderInstance_s; name:pcchar):s32;cdecl;external;
  function shaderProgramInit(sp:PshaderProgram_s):s32;cdecl;external;
  function shaderProgramFree(sp:PshaderProgram_s):s32;cdecl;external;
  function shaderProgramSetVsh(sp:PshaderProgram_s; dvle:PDVLE_s):s32;cdecl;external;
  function shaderProgramSetGsh(sp:PshaderProgram_s; dvle:PDVLE_s; stride:u8):s32;cdecl;external;
  function shaderProgramUse(sp:PshaderProgram_s):s32;cdecl;external;
