//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved
//

  type
    DVLE_type = (VERTEX_SHDR := GPU_VERTEX_SHADER,GEOMETRY_SHDR := GPU_GEOMETRY_SHADER);

    DVLE_constantType = (DVLE_CONST_BOOL := $0,DVLE_CONST_u8 := $1, DVLE_CONST_FLOAT24 := $2);

    DVLE_outputAttribute_t = (RESULT_POSITION := $0,RESULT_NORMALQUAT := $1,
      RESULT_COLOR := $2,RESULT_TEXCOORD0 := $3,
      RESULT_TEXCOORD0W := $4,RESULT_TEXCOORD1 := $5,
      RESULT_TEXCOORD2 := $6,RESULT_VIEW := $8
      );

    DVLP_s = record
        codeSize : u32;
        codeData : ^u32;
        opdescSize : u32;
        opcdescData : ^u32;
      end;

    DVLE_constEntry_s = record
        _type : u16;
        id : u16;
        data : array[0..3] of u32;
      end;

    DVLE_outEntry_s = record
        _type : u16;
        regID : u16;
        mask : u8;
        unk : array[0..2] of u8;
      end;

    DVLE_uniformEntry_s = record
        symbolOffset : u32;
        startReg : u16;
        endReg : u16;
      end;

    DVLE_s = record
        _type : DVLE_type;
        dvlp : ^DVLP_s;
        mainOffset : u32;
        endmainOffset : u32;
        constTableSize : u32;
        constTableData : ^DVLE_constEntry_s;
        outTableSize : u32;
        outTableData : ^DVLE_outEntry_s;
        uniformTableSize : u32;
        uniformTableData : ^DVLE_uniformEntry_s;
        symbolTableData : ^cchar;
        outmapMask : u8;
        outmapData : array[0..7] of u32;
      end;
    PDVLE_s  = ^DVLE_s;	

    DVLB_s = record
        numDVLE : u32;
        DVLP : DVLP_s;
        DVLE : ^DVLE_s;
      end;
    PDVLB_s  = ^DVLB_s;

  function DVLB_ParseFile(shbinData:Pu32; shbinSize:u32):PDVLB_s;cdecl;external;
  procedure DVLB_Free(dvlb:PDVLB_s);cdecl;external;
  function DVLE_GetUniformRegister(dvle:PDVLE_s; name:pcchar):s8;cdecl;external;
  procedure DVLE_GenerateOutmap(dvle:PDVLE_s);cdecl;external;
