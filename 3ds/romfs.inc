//the Pascal translation of libctru headers files for the nintendo 3ds platform 
// c file & header origin author (fincs)
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved.
//

  type
    romfs_header = record
        headerSize : u32;
        dirHashTableOff : u32;
        dirHashTableSize : u32;
        dirTableOff : u32;
        dirTableSize : u32;
        fileHashTableOff : u32;
        fileHashTableSize : u32;
        fileTableOff : u32;
        fileTableSize : u32;
        fileDataOff : u32;
      end;

    romfs_dir = record
        parent : u32;
        sibling : u32;
        childDir : u32;
        childFile : u32;
        nextHash : u32;
        nameLen : u32;
        name : ^u16;
      end;

    romfs_file = record
        parent : u32;
        sibling : u32;
        dataOff : u64;
        dataSize : u64;
        nextHash : u32;
        nameLen : u32;
        name : ^u16;
      end;

  function romfsInit:s32;cdecl;external;
  function romfsExit:s32;cdecl;external;
