//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved
//

  function acInit:s32;cdecl;external;
  function acExit:s32;cdecl;external;
  function ACU_GetWifiStatus(servhandle:pHandle; Wifiout:pu32):s32;cdecl;external;
  function ACU_WaitInternetConnection:s32;cdecl;external;
