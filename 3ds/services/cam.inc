//the Pascal translation of libctru headers files for the nintendo 3ds platform
// c file & header origin author (Steveice10)
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved.
//

  Type

  {*
   * @file cam.h
   * @brief CAM service for using the 3DS's front and back cameras.
    }


  {*
   * @brief Camera connection target ports.
    }
  { Port combinations. }

  type
    CAMU_Port = (PORT_NONE := $0,PORT_CAM1 := BIT(0),PORT_CAM2 := BIT(1),
      PORT_BOTH := PORT_CAM1 or PORT_CAM2);
  {*
   * @brief Camera combinations.
    }
  { Camera combinations. }

    CAMU_CameraSelect = (SELECT_NONE := $0,SELECT_OUT1 := BIT(0),SELECT_IN1 := BIT(1),
      SELECT_OUT2 := BIT(2),SELECT_IN1_OUT1 := SELECT_OUT1 or SELECT_IN1,
      SELECT_OUT1_OUT2 := SELECT_OUT1 or SELECT_OUT2,SELECT_IN1_OUT2 := SELECT_IN1 or SELECT_OUT2,
      SELECT_ALL := (SELECT_OUT1 or SELECT_IN1) or SELECT_OUT2);
    PCAMU_CameraSelect  = ^CAMU_CameraSelect;
  {*
   * @brief Camera contexts.
    }
  { Context combinations. }

    CAMU_Context = (CONTEXT_NONE := $0,CONTEXT_A := BIT(0),CONTEXT_B := BIT(1),
      CONTEXT_BOTH := CONTEXT_A or CONTEXT_B);
  {*
   * @brief Ways to flip the camera image.
    }

    CAMU_Flip = (FLIP_NONE := $0,FLIP_HORIZONTAL := $1,
      FLIP_VERTICAL := $2,FLIP_REVERSE := $3
      );
  {*
   * @brief Camera image resolutions.
    }
  { Alias for bottom screen to match top screen naming. }

    CAMU_Size = (SIZE_VGA := $0,SIZE_QVGA := $1,SIZE_QQVGA := $2,
      SIZE_CIF := $3,SIZE_QCIF := $4,SIZE_DS_LCD := $5,
      SIZE_DS_LCDx4 := $6,SIZE_CTR_TOP_LCD := $7,
      SIZE_CTR_BOTTOM_LCD := SIZE_QVGA);
  {*
   * @brief Camera capture frame rates.
    }

    CAMU_FrameRate = (FRAME_RATE_15 := $0,FRAME_RATE_15_TO_5 := $1,
      FRAME_RATE_15_TO_2 := $2,FRAME_RATE_10 := $3,
      FRAME_RATE_8_5 := $4,FRAME_RATE_5 := $5,
      FRAME_RATE_20 := $6,FRAME_RATE_20_TO_5 := $7,
      FRAME_RATE_30 := $8,FRAME_RATE_30_TO_5 := $9,
      FRAME_RATE_15_TO_10 := $A,FRAME_RATE_20_TO_10 := $B,
      FRAME_RATE_30_TO_10 := $C);
  {*
   * @brief Camera white balance modes.
    }
  { White balance aliases. }

    CAMU_WhiteBalance = (WHITE_BALANCE_AUTO := $0,WHITE_BALANCE_3200K := $1,
      WHITE_BALANCE_4150K := $2,WHITE_BALANCE_5200K := $3,
      WHITE_BALANCE_6000K := $4,WHITE_BALANCE_7000K := $5,
      WHITE_BALANCE_MAX := $6,WHITE_BALANCE_NORMAL := WHITE_BALANCE_AUTO,
      WHITE_BALANCE_TUNGSTEN := WHITE_BALANCE_3200K,WHITE_BALANCE_WHITE_FLUORESCENT_LIGHT := WHITE_BALANCE_4150K,
      WHITE_BALANCE_DAYLIGHT := WHITE_BALANCE_5200K,WHITE_BALANCE_CLOUDY := WHITE_BALANCE_6000K,
      WHITE_BALANCE_HORIZON := WHITE_BALANCE_6000K,WHITE_BALANCE_SHADE := WHITE_BALANCE_7000K
      );
  {*
   * @brief Camera photo modes.
    }

    CAMU_PhotoMode = (PHOTO_MODE_NORMAL := $0,PHOTO_MODE_PORTRAIT := $1,
      PHOTO_MODE_LANDSCAPE := $2,PHOTO_MODE_NIGHTVIEW := $3,
      PHOTO_MODE_LETTER := $4);
  {*
   * @brief Camera special effects.
    }

    CAMU_Effect = (EFFECT_NONE := $0,EFFECT_MONO := $1,EFFECT_SEPIA := $2,
      EFFECT_NEGATIVE := $3,EFFECT_NEGAFILM := $4,
      EFFECT_SEPIA01 := $5);
  {*
   * @brief Camera contrast patterns.
    }
  { Contrast aliases. }

    CAMU_Contrast = (CONTRAST_PATTERN_01 := $0,CONTRAST_PATTERN_02 := $1,
      CONTRAST_PATTERN_03 := $2,CONTRAST_PATTERN_04 := $3,
      CONTRAST_PATTERN_05 := $4,CONTRAST_PATTERN_06 := $5,
      CONTRAST_PATTERN_07 := $6,CONTRAST_PATTERN_08 := $7,
      CONTRAST_PATTERN_09 := $8,CONTRAST_PATTERN_10 := $9,
      CONTRAST_PATTERN_11 := $A,CONTRAST_LOW := CONTRAST_PATTERN_05,
      CONTRAST_NORMAL := CONTRAST_PATTERN_06,CONTRAST_HIGH := CONTRAST_PATTERN_07
      );
  {*
   * @brief Camera lens correction modes.
    }
  { Lens correction aliases. }

    CAMU_LensCorrection = (LENS_CORRECTION_OFF := $0,LENS_CORRECTION_ON_70 := $1,
      LENS_CORRECTION_ON_90 := $2,LENS_CORRECTION_DARK := LENS_CORRECTION_OFF,
      LENS_CORRECTION_NORMAL := LENS_CORRECTION_ON_70,LENS_CORRECTION_BRIGHT := LENS_CORRECTION_ON_90
      );
  {*
   * @brief Camera image output formats.
    }

    CAMU_OutputFormat = (OUTPUT_YUV_422 := $0,OUTPUT_RGB_565 := $1
      );
  {*
   * @brief Camera shutter sounds.
    }

    CAMU_ShutterSoundType = (SHUTTER_SOUND_TYPE_NORMAL := $0,SHUTTER_SOUND_TYPE_MOVIE := $1,
      SHUTTER_SOUND_TYPE_MOVIE_END := $2);
  {*
   * @brief Image quality calibration data.
    }
  {/< Auto exposure base target brightness. }
  {/< Left color correction matrix red normalization coefficient. }
  {/< Left color correction matrix green normalization coefficient. }
  {/< Left color correction matrix blue normalization coefficient. }
  {/< Color correction matrix position. }
  {/< Right camera, left color correction matrix red/green gain. }
  {/< Left camera, left color correction matrix red/green gain. }
  {/< Right camera, left color correction matrix blue/green gain. }
  {/< Left camera, left color correction matrix blue/green gain. }
  {/< Right camera, color correction matrix position threshold. }
  {/< Left camera, color correction matrix position threshold. }

    CAMU_ImageQualityCalibrationData = record
        aeBaseTarget : s16;
        kRL : s16;
        kGL : s16;
        kBL : s16;
        ccmPosition : s16;
        awbCcmL9Right : u16;
        awbCcmL9Left : u16;
        awbCcmL10Right : u16;
        awbCcmL10Left : u16;
        awbX0Right : u16;
        awbX0Left : u16;
      end;
  PCAMU_ImageQualityCalibrationData  = ^CAMU_ImageQualityCalibrationData;

  {*
   * @brief Stereo camera calibration data.
    }
  {/< #bool Whether the X and Y rotation data is valid. }
  {/< Padding. (Aligns isValidRotationXY to 4 bytes) }
  {/< Scale to match the left camera image with the right. }
  {/< Z axis rotation to match the left camera image with the right. }
  {/< X axis translation to match the left camera image with the right. }
  {/< Y axis translation to match the left camera image with the right. }
  {/< X axis rotation to match the left camera image with the right. }
  {/< Y axis rotation to match the left camera image with the right. }
  {/< Right camera angle of view. }
  {/< Left camera angle of view. }
  {/< Distance between cameras and measurement chart. }
  {/< Distance between left and right cameras. }
  {/< Image width. }
  {/< Image height. }
  {/< Reserved for future use. (unused) }

    CAMU_StereoCameraCalibrationData = record
        isValidRotationXY : u8;
        padding : array[0..2] of u8;
        scale : cfloat;
        rotationZ : cfloat;
        translationX : cfloat;
        translationY : cfloat;
        rotationX : cfloat;
        rotationY : cfloat;
        angleOfViewRight : cfloat;
        angleOfViewLeft : cfloat;
        distanceToChart : cfloat;
        distanceCameras : cfloat;
        imageWidth : s16;
        imageHeight : s16;
        reserved : array[0..15] of u8;
      end;
   PCAMU_StereoCameraCalibrationData  = ^CAMU_StereoCameraCalibrationData;

  {*
   * @brief Batch camera configuration for use without a context.
    }
  {/< #CAMU_CameraSelect Selected camera. }
  {/< Camera exposure. }
  {/< #CAMU_WhiteBalance Camera white balance. }
  {/< Camera sharpness. }
  {/< #bool Whether to automatically determine the proper exposure. }
  {/< #bool Whether to automatically determine the white balance mode. }
  {/< #CAMU_FrameRate Camera frame rate. }
  {/< #CAMU_PhotoMode Camera photo mode. }
  {/< #CAMU_Contrast Camera contrast. }
  {/< #CAMU_LensCorrection Camera lens correction. }
  {/< #bool Whether to enable the camera's noise filter. }
  {/< Padding. (Aligns last 3 fields to 4 bytes) }
  {/< X of the region to use for auto exposure. }
  {/< Y of the region to use for auto exposure. }
  {/< Width of the region to use for auto exposure. }
  {/< Height of the region to use for auto exposure. }
  {/< X of the region to use for auto white balance. }
  {/< Y of the region to use for auto white balance. }
  {/< Width of the region to use for auto white balance. }
  {/< Height of the region to use for auto white balance. }

    CAMU_PackageParameterCameraSelect = record
        camera : u8;
        exposure : s8;
        whiteBalance : u8;
        sharpness : s8;
        autoExposureOn : u8;
        autoWhiteBalanceOn : u8;
        frameRate : u8;
        photoMode : u8;
        contrast : u8;
        lensCorrection : u8;
        noiseFilterOn : u8;
        padding : u8;
        autoExposureWindowX : s16;
        autoExposureWindowY : s16;
        autoExposureWindowWidth : s16;
        autoExposureWindowHeight : s16;
        autoWhiteBalanceWindowX : s16;
        autoWhiteBalanceWindowY : s16;
        autoWhiteBalanceWindowWidth : s16;
        autoWhiteBalanceWindowHeight : s16;
      end;
  {*
   * @brief Batch camera configuration for use with a context.
    }
  {/< #CAMU_CameraSelect Selected camera. }
  {/< #CAMU_Context Selected context. }
  {/< #CAMU_Flip Camera image flip mode. }
  {/< #CAMU_Effect Camera image special effects. }
  {/< #CAMU_Size Camera image resolution. }

    CAMU_PackageParameterContext = record
        camera : u8;
        context : u8;
        flip : u8;
        effect : u8;
        size : u8;
      end;
  {*
   * @brief Batch camera configuration for use with a context and with detailed size information.
    }
  {/< #CAMU_CameraSelect Selected camera. }
  {/< #CAMU_Context Selected context. }
  {/< #CAMU_Flip Camera image flip mode. }
  {/< #CAMU_Effect Camera image special effects. }
  {/< Image width. }
  {/< Image height. }
  {/< First crop point X. }
  {/< First crop point Y. }
  {/< Second crop point X. }
  {/< Second crop point Y. }

    CAMU_PackageParameterContextDetail = record
        camera : u8;
        context : u8;
        flip : u8;
        effect : u8;
        width : s16;
        height : s16;
        cropX0 : s16;
        cropY0 : s16;
        cropX1 : s16;
        cropY1 : s16;
      end;
  {*
   * @brief Initializes the cam service.
   *
   * This will internally get the handle of the service, and on success call CAMU_DriverInitialize.
    }

  function camInit:s32;cdecl;external;

  {*
   * @brief Closes the cam service.
   *
   * This will internally call CAMU_DriverFinalize and close the handle of the service.
    }
  function camExit:s32;cdecl;external;

  {/ Begins capture on the specified camera port. }
  function CAMU_StartCapture(port:CAMU_Port):s32;cdecl;external;

  {/Terminates capture on the specified camera port. }
  function CAMU_StopCapture(port:CAMU_Port):s32;cdecl;external;

  {*
   * @brief Gets whether the specified camera port is busy.
   *
   * Writes the result to the provided output pointer.
    }
  function CAMU_IsBusy(busy:Pbool; port:CAMU_Port):s32;cdecl;external;

  {/Clears the buffer and error flags of the specified camera port. }
  function CAMU_ClearBuffer(port:CAMU_Port):s32;cdecl;external;

  {*
   * @brief Gets a handle to the event signaled on vsync interrupts.
   *
   * Writes the event handle to the provided output pointer.
    }
  function CAMU_GetVsyncInterruptEvent(event:PHandle; port:CAMU_Port):s32;cdecl;external;

  {*
   * @brief Gets a handle to the event signaled on camera buffer errors.
   *
   * Writes the event handle to the provided output pointer.
    }
  function CAMU_GetBufferErrorInterruptEvent(event:PHandle; port:CAMU_Port):s32;cdecl;external;

  {*
   * @brief Initiates the process of receiving a camera frame.
   *
   * Writes a completion event handle to the provided pointer and writes image data to the provided buffer.
    }
  function CAMU_SetReceiving(event:PHandle; dst:pointer; port:CAMU_Port; imageSize:u32; transferUnit:s16):s32;cdecl;external;

  {*
   * @brief Gets whether the specified camera port has finished receiving image data.
   *
   * Writes the result to the provided output pointer.
    }
  function CAMU_IsFinishedReceiving(finishedReceiving:Pbool; port:CAMU_Port):s32;cdecl;external;

  {/Sets the number of lines to transfer into an image buffer. }
  function CAMU_SetTransferLines(port:CAMU_Port; lines:s16; width:s16; height:s16):s32;cdecl;external;

  {*
   * @brief Gets the maximum number of lines that can be saved to an image buffer.
   *
   * Writes the result to the provided output pointer.
    }
  function CAMU_GetMaxLines(maxLines:Ps16; width:s16; height:s16):s32;cdecl;external;

  {/Sets the number of bytes to transfer into an image buffer. }
  function CAMU_SetTransferBytes(port:CAMU_Port; bytes:u32; width:s16; height:s16):s32;cdecl;external;

  {*
   * @brief Gets the number of bytes to transfer into an image buffer.
   *
   * Writes the s32 to the provided output pointer.
    }
  function CAMU_GetTransferBytes(transferBytes:Pu32; port:CAMU_Port):s32;cdecl;external;

  {*
   * @brief Gets the maximum number of bytes that can be saved to an image buffer.
   *
   * Writes the s32 to the provided output pointer.
    }
  function CAMU_GetMaxBytes(maxBytes:Pu32; width:s16; height:s16):s32;cdecl;external;

  {/Sets whether image trimming is enabled. }
  function CAMU_SetTrimming(port:CAMU_Port; trimming:bool):s32;cdecl;external;

  {*
   * @brief Gets whether image trimming is enabled.
   *
   * Writes the s32 to the provided output pointer.
    }
  function CAMU_IsTrimming(trimming:Pbool; port:CAMU_Port):s32;cdecl;external;

  {/Sets the parameters used for trimming images. }
  function CAMU_SetTrimmingParams(port:CAMU_Port; xStart:s16; yStart:s16; xEnd:s16; yEnd:s16):s32;cdecl;external;

  {*
   * @brief Gets the parameters used for trimming images.
   *
   * Writes the s32 to the provided output pointer.
    }
  function CAMU_GetTrimmingParams(xStart:Ps16; yStart:Ps16; xEnd:Ps16; yEnd:Ps16; port:CAMU_Port):s32;cdecl;external;

  {/Sets the parameters used for trimming images, relative to the center of the image. }
  function CAMU_SetTrimmingParamsCenter(port:CAMU_Port; trimWidth:s16; trimHeight:s16; camWidth:s16; camHeight:s16):s32;cdecl;external;

  {/Activates the specified camera. }
  function CAMU_Activate(select:CAMU_CameraSelect):s32;cdecl;external;

  {/Switches the specified camera's active context. }
  function CAMU_SwitchContext(select:CAMU_CameraSelect; context:CAMU_Context):s32;cdecl;external;

  {/Sets the exposure value of the specified camera. }
  function CAMU_SetExposure(select:CAMU_CameraSelect; exposure:s8):s32;cdecl;external;

  {/Sets the white balance mode of the specified camera. }
  function CAMU_SetWhiteBalance(select:CAMU_CameraSelect; whiteBalance:CAMU_WhiteBalance):s32;cdecl;external;

  {*
   * @brief Sets the white balance mode of the specified camera.
   *
   * TODO: Explain "without base up"?
    }
  function CAMU_SetWhiteBalanceWithoutBaseUp(select:CAMU_CameraSelect; whiteBalance:CAMU_WhiteBalance):s32;cdecl;external;

  {/Sets the sharpness of the specified camera. }
  function CAMU_SetSharpness(select:CAMU_CameraSelect; sharpness:s8):s32;cdecl;external;

  {/Sets whether auto exposure is enabled on the specified camera. }
  function CAMU_SetAutoExposure(select:CAMU_CameraSelect; autoExposure:bool):s32;cdecl;external;

  {*
   * @brief Gets whether auto exposure is enabled on the specified camera.
   *
   * Writes the result to the provided output pointer.
    }
  function CAMU_IsAutoExposure(autoExposure:Pbool; select:CAMU_CameraSelect):s32;cdecl;external;

  {/Sets whether auto white balance is enabled on the specified camera. }
  function CAMU_SetAutoWhiteBalance(select:CAMU_CameraSelect; autoWhiteBalance:bool):s32;cdecl;external;

  {*
   * @brief Gets whether auto white balance is enabled on the specified camera.
   *
   * Writes the s32 to the provided output pointer.
    }
  function CAMU_IsAutoWhiteBalance(autoWhiteBalance:Pbool; select:CAMU_CameraSelect):s32;cdecl;external;

  {/Flips the image of the specified camera in the specified context. }
  function CAMU_FlipImage(select:CAMU_CameraSelect; flip:CAMU_Flip; context:CAMU_Context):s32;cdecl;external;

  {/Sets the image resolution of the given camera in the given context, in detail. }
  function CAMU_SetDetailSize(select:CAMU_CameraSelect; width:s16; height:s16; cropX0:s16; cropY0:s16; 
             cropX1:s16; cropY1:s16; context:CAMU_Context):s32;cdecl;external;

  {/Sets the image resolution of the given camera in the given context. }
  function CAMU_SetSize(select:CAMU_CameraSelect; size:CAMU_Size; context:CAMU_Context):s32;cdecl;external;

  {/Sets the frame rate of the given camera. }
  function CAMU_SetFrameRate(select:CAMU_CameraSelect; frameRate:CAMU_FrameRate):s32;cdecl;external;

  {/Sets the photo mode of the given camera. }
  function CAMU_SetPhotoMode(select:CAMU_CameraSelect; photoMode:CAMU_PhotoMode):s32;cdecl;external;

  {/Sets the special effects of the given camera in the given context. }
  function CAMU_SetEffect(select:CAMU_CameraSelect; effect:CAMU_Effect; context:CAMU_Context):s32;cdecl;external;

  {/Sets the contrast mode of the given camera. }
  function CAMU_SetContrast(select:CAMU_CameraSelect; contrast:CAMU_Contrast):s32;cdecl;external;

  {/Sets the lens correction mode of the given camera. }
  function CAMU_SetLensCorrection(select:CAMU_CameraSelect; lensCorrection:CAMU_LensCorrection):s32;cdecl;external;

  {/Sets the output format of the given camera in the given context. }
  function CAMU_SetOutputFormat(select:CAMU_CameraSelect; format:CAMU_OutputFormat; context:CAMU_Context):s32;cdecl;external;

  {/Sets the region to base auto exposure off of for the specified camera. }
  function CAMU_SetAutoExposureWindow(select:CAMU_CameraSelect; x:s16; y:s16; width:s16; height:s16):s32;cdecl;external;

  {/Sets the region to base auto white balance off of for the specified camera. }
  function CAMU_SetAutoWhiteBalanceWindow(select:CAMU_CameraSelect; x:s16; y:s16; width:s16; height:s16):s32;cdecl;external;

  {/Sets whether the specified camera's noise filter is enabled. }
  function CAMU_SetNoiseFilter(select:CAMU_CameraSelect; noiseFilter:bool):s32;cdecl;external;

  {/Synchronizes the specified cameras' vsync timing. }
  function CAMU_SynchronizeVsyncTiming(select1:CAMU_CameraSelect; select2:CAMU_CameraSelect):s32;cdecl;external;

  {*
   * @brief Gets the vsync timing record of the specified camera for the specified number of signals.
   *
   * Writes the result to the provided output pointer, which should be of size "past * sizeof(s64)".
    }
  function CAMU_GetLatestVsyncTiming(timing:Ps64; port:CAMU_Port; past:u32):s32;cdecl;external;

  {*
   * @brief Gets the specified camera's stereo camera calibration data.
   *
   * Writes the result to the provided output pointer.
    }
  function CAMU_GetStereoCameraCalibrationData(data:PCAMU_StereoCameraCalibrationData):s32;cdecl;external;

  {/Sets the specified camera's stereo camera calibration data. }
  function CAMU_SetStereoCameraCalibrationData(data:CAMU_StereoCameraCalibrationData):s32;cdecl;external;

  {*
   * @brief Writes to the specified I2C register of the specified camera.
   *
   * Use with caution.
    }
  function CAMU_WriteRegisterI2c(select:CAMU_CameraSelect; addr:u16; data:u16):s32;cdecl;external;

  {*
   * @brief Writes to the specified MCU variable of the specified camera.
   *
   * Use with caution.
    }
  function CAMU_WriteMcuVariableI2c(select:CAMU_CameraSelect; addr:u16; data:u16):s32;cdecl;external;

  {*
   * @brief Reads the specified I2C register of the specified camera.
   *
   * Writes the result to the provided output pointer.
    }
  function CAMU_ReadRegisterI2cExclusive(data:Pu16; select:CAMU_CameraSelect; addr:u16):s32;cdecl;external;

  {*
   * @brief Reads the specified MCU variable of the specified camera.
   *
   * Writes the result to the provided output pointer.
    }
  function CAMU_ReadMcuVariableI2cExclusive(data:Pu16; select:CAMU_CameraSelect; addr:u16):s32;cdecl;external;

  {*
   * @brief Sets the specified camera's image quality calibration data.
    }
  function CAMU_SetImageQualityCalibrationData(data:CAMU_ImageQualityCalibrationData):s32;cdecl;external;

  {*
   * @brief Gets the specified camera's image quality calibration data.
   *
   * Writes the result to the provided output pointer.
    }
  function CAMU_GetImageQualityCalibrationData(data:PCAMU_ImageQualityCalibrationData):s32;cdecl;external;

  {/Configures a camera with pre-packaged configuration data without a context. }
  function CAMU_SetPackageParameterWithoutContext(param:CAMU_PackageParameterCameraSelect):s32;cdecl;external;

  {/Configures a camera with pre-packaged configuration data with a context. }
  function CAMU_SetPackageParameterWithContext(param:CAMU_PackageParameterContext):s32;cdecl;external;

  {/Configures a camera with pre-packaged configuration data without a context and extra resolution details. }
  function CAMU_SetPackageParameterWithContextDetail(param:CAMU_PackageParameterContextDetail):s32;cdecl;external;

  {/Gets the Y2R coefficient applied to image data by the camera. }
  function CAMU_GetSuitableY2rStandardCoefficient(coefficient:PY2R_StandardCoefficient):s32;cdecl;external;

  {/Plays the specified shutter sound. }
  function CAMU_PlayShutterSound(sound:CAMU_ShutterSoundType):s32;cdecl;external;

  {/Initializes the camera driver. }
  function CAMU_DriverInitialize:s32;cdecl;external;

  {/Finalizes the camera driver. }
  function CAMU_DriverFinalize:s32;cdecl;external;

  {*
   * @brief Gets the current activated camera.
   *
   * Writes the result to the provided output pointer.
    }
  function CAMU_GetActivatedCamera(select:PCAMU_CameraSelect):s32;cdecl;external;

  {*
   * @brief Gets the current sleep camera.
   *
   * Writes the result to the provided output pointer.
    }
  function CAMU_GetSleepCamera(select:PCAMU_CameraSelect):s32;cdecl;external;

  {/Sets the current sleep camera. }
  function CAMU_SetSleepCamera(select:CAMU_CameraSelect):s32;cdecl;external;

  {/Sets whether to enable synchronization of left and right camera brightnesses. }
  function CAMU_SetBrightnessSynchronization(brightnessSynchronization:bool):s32;cdecl;external;

