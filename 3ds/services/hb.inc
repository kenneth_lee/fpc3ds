//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved.
//

  function hbInit:s32;cdecl;external;
  procedure hbExit;cdecl;external;

  { flushes/invalidates entire data/instruction cache }
  { can be useful when writing code to executable pages }
  function HB_FlushInvalidateCache:s32;cdecl;external;

  { fetches the address for ninjhax bootloader addresses, useful for running 3dsx executables }
  { void (*callBootloader)(Handle hb, Handle file); }
  { void (*setArgs)(u32* src, u32 length); }
  function HB_GetBootloaderAddresses(load3dsx:Ppointer; setArgv:Ppointer):s32;cdecl;external;

  { changes the permissions of a given number of pages at address addr to mode }
  { should it fail, the appropriate kernel error code will be returned and *reprotectedPages (if not NULL) }
  { will be set to the number of sequential pages which were successfully reprotected + 1 }
  function HB_ReprotectMemory(addr:Pu32; pages:u32; mode:u32; reprotectedPages:Pu32):s32;cdecl;external;
