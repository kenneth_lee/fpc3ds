//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved
//

  function ptmInit:s32;cdecl;external;
  function ptmExit:s32;cdecl;external;
  function PTMU_GetBatteryLevel(servhandle:pHandle; blout:pu8):s32;cdecl;external;
  function PTMU_GetBatteryChargeState(servhandle:pHandle; bcout:pu8):s32;cdecl;external;
