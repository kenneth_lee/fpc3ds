//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved.
//

  type
    qtmHeadtrackingInfoCoord = record
        x : cfloat;
        y : cfloat;
      end;
    PqtmHeadtrackingInfoCoord  = ^qtmHeadtrackingInfoCoord;

    qtmHeadtrackingInfo = record
        flags : array[0..4] of u8;
        padding : array[0..2] of u8;
        floatdata_x08 : cfloat;
        coords0 : array[0..3] of qtmHeadtrackingInfoCoord;
        unk_x2c : array[0..4] of u32;
      end;
    PqtmHeadtrackingInfo  = ^qtmHeadtrackingInfo;

  function qtmInit:s32;cdecl;external;
  procedure qtmExit;cdecl;external;
  function qtmCheckInitialized:bool;cdecl;external;
  function qtmGetHeadtrackingInfo(val:u64; htInfo:PqtmHeadtrackingInfo):s32;cdecl;external;

  {val is normally 0. }
  function qtmCheckHeadFullyDetected(info:PqtmHeadtrackingInfo):bool;cdecl;external;
  function qtmConvertCoordToScreen(coord:PqtmHeadtrackingInfoCoord; screen_width: ^cfloat; screen_height: ^cfloat; x:Pu32; y:Pu32):s32;cdecl;external;

  {screen_* can be NULL to use the default values for the top-screen. }
