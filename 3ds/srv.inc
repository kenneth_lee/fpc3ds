//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved
//

  function srvInit:s32;cdecl;external;
  function srvExit:s32;cdecl;external;
  function srvRegisterClient:s32;cdecl;external;
  function srvGetServiceHandle(SrvHandle:pHandle; name:pchar):s32;cdecl;external;
  function srvPmInit:s32;cdecl;external;
  function srvRegisterProcess(procid:u32; count:u32; serviceaccesscontrol:pointer):s32;cdecl;external;
  function srvUnregisterProcess(procid:u32):s32;cdecl;external;
