//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved
//

{$ifdef 3dsintf}

{$IFDEF FPC}
{$PACKRECORDS C}
{$ENDIF}

  type


    rbtree_node = record
        parent_color : pcuint; // {uintptr_t};cdecl;
        child : array[0..1] of ^rbtree_node;
      end;
    rbtree_node_t = rbtree_node;
    Prbtree_node_t  = ^rbtree_node_t;


    rbtree_node_destructor_t = procedure (Node:Prbtree_node_t);cdecl;
    rbtree_node_comparator_t = function (lhs:Prbtree_node_t; rhs:Prbtree_node_t):cint;cdecl;

    rbtree = record
        root : ^rbtree_node_t;
        comparator : rbtree_node_comparator_t;
        size : u32;
      end;
    rbtree_t = rbtree;
    Prbtree_t  = ^rbtree_t;

  function rbtree_item(ptr,_type,member : longint) : Prbtree_node_t;

  procedure rbtree_init(tree:Prbtree_t; comparator:rbtree_node_comparator_t);cdecl;external;
  function rbtree_empty(tree:Prbtree_t):cint;cdecl;external;
  function rbtree_size(tree:Prbtree_t):u32;cdecl;external;
  function rbtree_insert(tree:Prbtree_t; node:Prbtree_node_t):rbtree_node_t ;cdecl;external;
  procedure rbtree_insert_multi(tree:Prbtree_t; node:Prbtree_node_t);cdecl;external;
  function rbtree_find(tree:Prbtree_t; node:Prbtree_node_t):Prbtree_node_t;cdecl;external;
  function rbtree_min(tree:Prbtree_t):Prbtree_node_t;cdecl;external;
  function rbtree_max(tree:Prbtree_t):Prbtree_node_t;cdecl;external;
  function rbtree_node_next(node:Prbtree_node_t):Prbtree_node_t;cdecl;external;
  function rbtree_node_prev(node:Prbtree_node_t):Prbtree_node_t;cdecl;external;
  function rbtree_remove(tree:Prbtree_t; node:Prbtree_node_t; destruct:rbtree_node_destructor_t):Prbtree_node_t;cdecl;external;
  procedure rbtree_clear(tree:Prbtree_t; destruct:rbtree_node_destructor_t);cdecl;external;{$endif 3dsintf}

{$ifdef 3dsimpl}
  function rbtree_item(ptr,_type,member : longint) : Prbtree_node_t;
    begin
      // rbtree_item:=Prbtree_node_t((pchar(ptr))-(offsetof(_type,member)));
    end;
{$endif 3dsimpl}
