//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved.
//

  {
   *
   *  @param[vout] vout Output codepoint
   *  @param[vin]  vin  Input sequence
   *
   *  @returns number of input code units consumed
   *  @returns -1 for error
    }
function decode_utf8(vout:pu32; vin:pu8):u32;cdecl;external;
function decode_utf16(vout:pu32; vin:pu16):u32;cdecl;external;
function encode_utf8(vout:pu8; vin:pu32):u32;cdecl;external;
function encode_utf16(vout:pu16; vin:pu32):u32;cdecl;external;
function utf8_to_utf16(vout:pu16; vin:pu8; len:u32):u32;cdecl;external;
function utf8_to_utf32(vout:pu32; vin:pu8; len:u32):u32;cdecl;external;
function utf16_to_utf8(vout:pu8; vin:pu16; len:u32):u32;cdecl;external;
function utf16_to_utf32(vout:pu32; vin:pu16; len:u32):u32;cdecl;external;
function utf32_to_utf8(vout:pu8; vin:pu32; len:u32):u32;cdecl;external;
function utf32_to_utf16(vout:pu16; vin:pu32; len:u32):u32;cdecl;external;
