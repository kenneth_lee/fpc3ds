//the Pascal translation of libctru headers files for the nintendo 3ds platform
//
// Copyright (c) 2015 Kenny D. Lee
// all rights reserved

  function vramAlloc(size:s32):pointer;cdecl;external;
  function vramMemAlign(size:s32; alignment:s32):pointer;cdecl;external;   { returns a 16-byte aligned address }
  function vramRealloc(mem:pointer; size:s32):pointer;cdecl;external;
  procedure vramFree(mem:pointer);cdecl;external;
  function vramSpaceFree:u32;cdecl;external;
